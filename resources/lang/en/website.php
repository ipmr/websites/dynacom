<?php

return [

    'home' => 'Home',
    'about' => 'About us',
    'services' => 'Services',
    'solutions' => 'Solutions',
    'contact' => 'Contact',
    'login' => 'Login',
    'logout' => 'Logout'
];
