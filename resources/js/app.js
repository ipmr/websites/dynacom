require('./bootstrap');
import Vue from 'vue';

// Plugins
import Vue2Editor from "vue2-editor";
Vue.use(Vue2Editor);


// Components
import PageForm from './components/PageForm';
import HomeCarousel from './components/HomeCarousel';
import SolutionsIcons from './components/SolutionsIcons';
import DynacomOverview from './components/DynacomOverview';
import DynacomVideo from './components/DynacomVideo';
import SubMenuLink from './components/SubMenuLink';

const app = new Vue({
    el: '#app',
    components: {
    	PageForm,
    	HomeCarousel,
    	SolutionsIcons,
    	DynacomOverview,
    	DynacomVideo,
    	SubMenuLink,
    }
});