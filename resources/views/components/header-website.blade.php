<header class="bg-white shadow-sm header-website">
	
	@include('components.preheader-website')

	<div class="d-flex align-items-center justify-content-center header-website-content">
		<div class="container d-flex align-items-center">
			<a href="{{ route('website') }}" class="logo">
				<img src="{{ asset('img/svg/logo_hz.svg') }}" alt="">
			</a>
			<nav class="mx-auto">
				<a href="#">@lang('website.home')</a>
				<a href="#">@lang('website.about')</a>
				<a href="#">@lang('website.services')</a>
				<sub-menu-link title="{{ __('website.solutions') }}">
					<template #default>
						<div class="row d-flex align-items-center">
							<div class="col-sm-6">
								<h2 class="h3 mb-4">Our Solutions</h2>
								<p>
									We always integrate the best IT solutions on the market 
									that bring value to your business, throughout our more than 
									<b>{{ config('app.since_year') }} years</b> on the business we have managed to incorporate the 
									right solution portfolio that will be able to suit your IT needs.
								</p>
								<nav class="d-flex flex-column mt-4">
									<a href="/p/collaboration-solutions" class="text-primary font-weight-bold m-0 mb-2">Collaboration Solutions</a>
									<a href="/p/data-center-solutions" class="text-primary font-weight-bold m-0 mb-2">Data Center Solutions</a>
									<a href="/p/it-solutions" class="text-primary font-weight-bold m-0 mb-2">IT Solutions</a>
									<a href="#" class="text-primary font-weight-bold m-0 mb-2">Software Solutions</a>
									<a href="#" class="text-primary font-weight-bold m-0 mb-2">Electronic Security Solutions</a>
									<a href="#" class="text-primary font-weight-bold m-0 mb-2">Network Security Solutions</a>
									<a href="#" class="text-primary font-weight-bold m-0 mb-2">Access and Wireless</a>
								</nav>
							</div>
							<div class="col-sm-6">
								<img src="{{ asset('img/svg/illustrations/undraw_interview_rmcf.svg') }}" class="img-fluid" alt="">
							</div>
						</div>
					</template>
				</sub-menu-link>
				<a href="#">@lang('website.contact')</a>
			</nav>
			<nav>
				<a href="#"><i class="fa fa-search fa-sm"></i></a>
				<a href="#"><i class="fa fa-shopping-cart fa-sm"></i></a>
				@guest
					<a href="{{ route('login') }}"><i class="fa fa-user fa-sm"></i></a>
				@else
					<a href="{{ route('logout') }}"
					    onclick="event.preventDefault();
					    document.getElementById('logout-form').submit();">
					    <i class="fa fa-power-off fa-sm"></i>
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					    @csrf
					</form>
				@endguest
			</nav>
		</div>
	</div>

</header>