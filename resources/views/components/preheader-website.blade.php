<header class="preheader-website d-flex justify-content-center align-items-center">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-sm-6">
				<span class="text-white font-weight-bold">
					<i class="fa fa-phone text-danger mr-2"></i>

					CONTACT US: 01 800 123 4567
				</span>
			</div>
			<div class="col-sm-6 text-right">
				<a href="#" class="btn custom-btn custom-btn-white">
					<i class="fa fa-headset fa-lg mr-2"></i>
					Customer Support
				</a>
			</div>
		</div>
	</div>
</header>