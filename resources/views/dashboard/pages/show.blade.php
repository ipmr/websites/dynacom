@extends('layouts.dashboard', ['title' => $page->title])
@section('body')
	
	<div class="row">
		<div class="col-sm-8">
			<div class="card card-body shadow-sm">
				<page-form :page="{{ $page }}"></page-form>
			</div>
		</div>
	</div>

@stop