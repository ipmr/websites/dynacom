@extends('layouts.dashboard', ['title' => 'Nueva página'])

@section('body')

	
	<div class="row">
		<div class="col-sm-8">
			<div class="card card-body shadow-sm">
				<page-form></page-form>
			</div>
		</div>
	</div>


@stop