@extends('layouts.dashboard', ['title' => 'Páginas'])
@section('body')
	@if(count($pages))
		<a href="{{ route('pages.create') }}" class="btn btn-primary">Agregar nueva página</a>
		<table class="table m-0 bg-white shadow-sm mt-4 rounded">
			<thead>
				<tr>
					<th>Título</th>
					<th>Fecha de creación</th>
					<th>Última actualización</th>
					<th>Estado</th>
				</tr>
			</thead>
			<tbody>
				@foreach($pages as $page)
				<tr>
					<td>
						<a href="{{ route('pages.show', $page) }}">{{ $page->title }}</a>
					</td>
					<td>
						{{ $page->created_at->format('d M, Y h:i a') }}
					</td>
					<td>
						{{ $page->updated_at->format('d M, Y h:i a') }}
					</td>
					<td>
						{{ $page->status }}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	@else
		<div class="row">
			<div class="col-sm-6 mx-auto text-center">
				<p class="lead text-muted">No se han agregado páginas aún.</p>
				<a href="{{ route('pages.create') }}" class="btn btn-primary">Agregar nueva página</a>
			</div>
		</div>
	@endif
@stop