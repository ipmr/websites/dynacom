@include('components.header', ['layout_website' => true])
@include('components.header-website')
<section class="py-5">
	@yield('content')
</section>
@include('components.footer')