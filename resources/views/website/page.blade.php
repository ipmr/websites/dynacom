@extends('layouts.website')
@section('content')
	<section class="bg-pattern py-5">
		<div class="container position-relative">
			<h1 class="text-center text-white">{{ $page->title }}</h1>
		</div>
	</section>

	<section class="py-5">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 page-content">
					<p class="lead mb-5">{{ $page->subtitle }}</p>
					{!! $page->body !!}
				</div>
			</div>
		</div>
	</section>
@stop