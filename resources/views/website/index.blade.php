@extends('layouts.website')
@section('content')
	
	<!-- CAROUSEL -->
	<section class="py-5 bg-pattern">
		<div class="container">
			<home-carousel></home-carousel>
		</div>
	</section>
	<!-- END CAROUSEL -->
	
	<!-- ICONS -->
	<section class="py-5 bg-light shadow-sm">
		<div class="container">
			<solutions-icons></solutions-icons>
		</div>
	</section>
	<!-- END ICONS -->

	<!-- OVERVIEW -->
	<section class="py-5">
		<div class="container py-4">
			<div class="row">
				<div class="col-sm-7">
					<dynacom-overview></dynacom-overview>
				</div>
				<div class="col-sm-5">
					<dynacom-video></dynacom-video>
				</div>
			</div>
		</div>
	</section>
	<!-- END OVERVIEW -->

@stop