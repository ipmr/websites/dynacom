<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebsiteController@index')->name('website');
Route::get('/p/{page:slug}', 'WebsiteController@page');

Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function(){

	Route::get('/', 'DashboardController@index')->name('dashboard');
	Route::resource('pages', 'PageController');

});
