<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $page = request()->page ? request()->page : null;
        return [
            'title'     => ['required', 'string', Rule::unique('pages')->ignore($page ? $page->id : '')],
            'subtitle'  => 'string|nullable',
            'body'      => 'string|nullable',
            'status_id' => 'numeric|in:1,2,3',
        ];
    }

    public function messages()
    {
        return [

            'title.required'    => 'El título de la página es requerido',
            'title.unique'      => 'Ya existe una página con el mismo título, por favor intenta otro.',
            'subtitle.string'   => 'El subtítulo parece tener un formato invalido',
            'body.string'       => 'El contenido de la página no es válido',
            'status_id.numeric' => 'El estado de la página no es válido',
            'status_id.in'      => 'El estado de la página no es válido',

        ];
    }
}
