<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function index(){
    	return view('website.index');
    }

    public function page(Page $page){
    	if($page){
    		return view('website.page', compact('page'));
    	}else{
    		abort;
    	}
    }

}
