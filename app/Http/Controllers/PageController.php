<?php
namespace App\Http\Controllers;

use App\Http\Requests\PageRequest;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::orderBy('created_at')->get();
        return view('dashboard.pages.index', compact('pages'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pages.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {

        $user = auth()->user();

        $page            = new Page();
        $page->user_id   = $user->id;
        $page->title     = $request->title;
        $page->subtitle  = $request->subtitle;
        $page->body      = $request->body;
        $page->status_id = 1;
        $page->slug      = Str::slug($request->title);
        $page->save();

        $response = [
            'msg' => "Se ha guardado la página correctamente."
        ];

        return response($response);

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return view('dashboard.pages.show', compact('page'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, Page $page)
    {
        $page->title     = $request->title;
        $page->subtitle  = $request->subtitle;
        $page->body      = $request->body;
        $page->status_id = 1;
        $page->slug      = Str::slug($request->title);
        $page->save();

        $response = [
            'msg' => "La información de la página ha sido actualizada."
        ];

        return response($response);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }
}
