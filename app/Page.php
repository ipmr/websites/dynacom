<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Page extends Model
{
	public function getStatusAttribute(){
		$status = $this->status_id;
		switch ($status) {
			case 1:
				return "Borrador";
				break;
			
			case 2:
				return "Publicada";
				break;
		}
	}
}